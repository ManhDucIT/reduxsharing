import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import FoodListScreen from './app/screens/FoodListScreen';
import HomeScreen from './app/screens/HomeScreen';

const Stack = createStackNavigator();

const AppNavigator = () => {
 return (
   <Stack.Navigator>
     <Stack.Screen name='HomeScreen' component={HomeScreen} options={{ title: 'Home' }} />
     <Stack.Screen name='FoodListScreen' component={FoodListScreen} options={{ title: 'Food List' }} />
   </Stack.Navigator>
 );
}

export default AppNavigator;
