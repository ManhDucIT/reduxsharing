import React, { Component } from 'react';
import { Button, StyleSheet, Text, TextInput, View } from 'react-native';
import * as RootNavigation from '../../RootNavigation';

class HomeScreen extends Component {

  state = {
    food: null,
    foodList: []
  }

  submitFood = (food) => {
    this.setState(
      {
        foodList:
          [...this.state.foodList, {
            key: Math.random(),
            name: food
          }]
      }, () => {
        alert('Added succeessfully')

        this.setState({
          food: null
        })
      })

  }

  deleteFood = (key) => {
    this.setState(
      {
        foodList:
          [
            ...this.state.foodList.filter((item) =>
              item.key !== key)
          ]
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Without Redux</Text>

        <TextInput
          value={this.state.food}
          placeholder='Name'
          style={styles.foodInput}
          onChangeText={(food) => this.setState({ food })}/>

          <Button
            title='Submit'
            color='black'
            onPress={() => this.submitFood(this.state.food)}/>
            
        <View style={styles.buttonMargin}>
          <Button
            title='Go to FoodList'
            onPress={() =>
              RootNavigation.navigate('FoodListScreen',
                {
                  foodList: this.state.foodList,
                  deleteFood: this.deleteFood
                })
            }/>
        </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 16,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 30,
    marginBottom: 48
  },
  foodInput: {
    fontSize: 32,
    marginBottom: 32,
    borderWidth: 1,
    padding: 8,
    width: '80%',
    borderRadius: 10,
  },
  buttonMargin: {
    marginTop: 8
  }
});

export default HomeScreen;