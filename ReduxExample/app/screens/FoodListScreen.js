import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native';

class FoodListScreen extends Component {

  render() {
    return (
      <FlatList style={styles.listContainer}
        data={this.props.route.params.foodList}
        keyExtractor={(item, index) => item.key.toString()}
        renderItem={
          (data) =>
            <View style={styles.rowItem}>
              <Text style={styles.listText}>{data.item.name}</Text>
              <TouchableOpacity onPress={() => this.props.route.params.deleteFood(data.item.key)}>
                <Text style={styles.deleteAction}>Delete</Text>
              </TouchableOpacity>
            </View>
        }/>
    );
  }
};

const styles = StyleSheet.create({
  listContainer: {
    padding: 16
  },
  listText: {
    fontSize: 30
  },
  rowItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  rowTitle: {
    flex: 1
  },
  deleteAction: {
    color: 'red',
    fontSize: 18
  }
});

export default FoodListScreen;