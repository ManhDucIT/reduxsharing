import { NavigationContainer } from '@react-navigation/native';
import * as React from 'react';
import AppNavigator from './AppNavigator';
import { navigationRef } from './RootNavigation';

const App = () => {
  return (
   <NavigationContainer ref={navigationRef}>
    <AppNavigator />
   </NavigationContainer>
  );
 }
 
 export default App;